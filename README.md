# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This application will list down all the issues along with the user and also list the comments related to the issues


* Technical Stack - Android , Kotlin, Android Studio
* Dependence Injection - Koin
* Networking Library - Reftrofit
* Asynchronous Callback - Kotlin Coroutine
* Unit Testing -> Mockito

* Apk of the application -> https://bitbucket.org/shreyashmohta89/issuetracker/src/master/issueTracker.apk
* Video of the application -> https://bitbucket.org/shreyashmohta89/issuetracker/src/master/issueTracker.mov


* Application Version 1.0
