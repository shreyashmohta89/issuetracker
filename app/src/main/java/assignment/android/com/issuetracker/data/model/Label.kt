/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 6:48 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 6:48 PM
 *
 */

package assignment.android.com.issuetracker.data.model


import com.google.gson.annotations.SerializedName

data class Label(
    @SerializedName("color")
    val color: String,
    @SerializedName("default")
    val default: Boolean,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("node_id")
    val nodeId: String,
    @SerializedName("url")
    val url: String
)