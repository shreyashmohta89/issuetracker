/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 7:24 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 7:24 PM
 *
 */

package assignment.android.com.issuetracker.ui.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import assignment.android.com.issuetracker.R
import assignment.android.com.issuetracker.data.model.Issue
import assignment.android.com.issuetracker.databinding.ViewIssueItemBinding
import assignment.android.com.issuetracker.ui.activity.CommentActivity
import assignment.android.com.issuetracker.utils.ISSUE_NUMBER

class IssueAdapter(val context: Context, var issueList: List<Issue>?) :
    RecyclerView.Adapter<IssueAdapter.IssueViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueViewHolder {

        val viewIssueItemBinding: ViewIssueItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.view_issue_item, parent, false
        )
        return IssueViewHolder(viewIssueItemBinding.root)
    }

    override fun getItemCount(): Int {
        return issueList!!.size
    }

    override fun onBindViewHolder(holder: IssueViewHolder, position: Int) {
        val issueInfo: Issue = issueList!![position]
        holder.itemViewBinding?.issue = issueInfo

        holder.bind(issueInfo, context)

    }

    class IssueViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemViewBinding = DataBindingUtil.bind<ViewIssueItemBinding>(itemView)

        fun bind(issue: Issue, context: Context) {
            itemView.setOnClickListener {
                val intent = Intent(context, CommentActivity::class.java)
                intent.putExtra(ISSUE_NUMBER, issue.number)
                context.startActivity(intent)
            }
        }


    }

}