/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 8:05 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 8:05 PM
 *
 */

package assignment.android.com.issuetracker.data.model


import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("author_association")
    val authorAssociation: String,
    @SerializedName("body")
    val body: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("html_url")
    val htmlUrl: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("issue_url")
    val issueUrl: String,
    @SerializedName("node_id")
    val nodeId: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("user")
    val user: User
)