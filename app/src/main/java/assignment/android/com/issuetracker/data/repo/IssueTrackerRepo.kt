/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 6:57 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 6:57 PM
 *
 */

package assignment.android.com.issuetracker.data.repo

import androidx.lifecycle.LiveData
import assignment.android.com.issuetracker.data.api.ApiService
import assignment.android.com.issuetracker.data.model.Comment
import assignment.android.com.issuetracker.data.model.Issue
import assignment.android.com.issuetracker.utils.NetworkState

class IssueTrackerRepo(private val apiService : ApiService){
    lateinit var issueTrackerRemoteDataSource: IssueTrackerRemoteDataSource

    /**
     * This function will return the LiveData of the list of issues
     */
    fun getIssues(): LiveData<List<Issue>> {

        issueTrackerRemoteDataSource = IssueTrackerRemoteDataSource(apiService)
        return issueTrackerRemoteDataSource.getIssues()

    }

    /**
     * This function will return the LiveData of the list of comments
     */
    fun getCommentsById(issueId : Int): LiveData<List<Comment>> {

        issueTrackerRemoteDataSource = IssueTrackerRemoteDataSource(apiService)
        return issueTrackerRemoteDataSource.getCommentById(issueId)

    }

    /**
     * This function will return the network State
     */
    fun getNetworkState(): LiveData<NetworkState> {
        return issueTrackerRemoteDataSource.networkState
    }

    fun cancelJobsIssue() {
        issueTrackerRemoteDataSource.cancelJobIssue()
    }

    fun cancelJobsComments() {
        issueTrackerRemoteDataSource.cancelJobComments()
    }
}