/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 8:22 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 8:22 PM
 *
 */

package assignment.android.com.issuetracker.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import assignment.android.com.issuetracker.data.model.Issue
import assignment.android.com.issuetracker.data.repo.IssueTrackerRepo
import assignment.android.com.issuetracker.utils.NetworkState

class IssueViewModel(private val issueTrackerRepo: IssueTrackerRepo): ViewModel(){


    lateinit var issueList: LiveData<List<Issue>>

    val networkState: LiveData<NetworkState> by lazy {
        issueTrackerRepo.getNetworkState()
    }

    fun getIssues(): LiveData<List<Issue>> {
        issueList = issueTrackerRepo.getIssues()
        return issueList
    }

    fun cancelJobsIssue() {
        issueTrackerRepo.cancelJobsIssue()
    }

}