/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 6:51 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 6:51 PM
 *
 */

package assignment.android.com.issuetracker.data.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import assignment.android.com.issuetracker.data.api.ApiService
import assignment.android.com.issuetracker.data.model.Comment
import assignment.android.com.issuetracker.data.model.Issue
import assignment.android.com.issuetracker.utils.NetworkState
import kotlinx.coroutines.*

class IssueTrackerRemoteDataSource(private val apiService: ApiService){

    private val TAG = "IssueTrackerDataSource"

    var jobIssue: CompletableJob? = null

    var jobComments: CompletableJob? = null

    private val _networkState = MutableLiveData<NetworkState>()

    val networkState: LiveData<NetworkState>
        get() = _networkState

    /**
     * This function will get the list of all issues
     */
    fun getIssues() : LiveData<List<Issue>>{
        _networkState.postValue(NetworkState.LOADING)
        jobIssue = Job()
        return object : LiveData<List<Issue>>() {
            override fun onActive() {
                super.onActive()
                jobIssue?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try {
                            val issue = apiService.getIssues()
                            withContext(Dispatchers.Main) {
                                value = issue
                                Log.e(TAG, "Issues size-> ${value?.size}")
                                Log.e(TAG, "Issues -> $value")
                                _networkState.postValue(NetworkState.LOADED)
                                theJob.complete()
                            }

                        } catch (throwable: Throwable) {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e(TAG, "Error -> ${throwable.message}")
                        }
                    }
                }
            }
        }
    }


    /**
     * This function will list all the comments for a particular issues
     */
    fun getCommentById(issueId : Int) : LiveData<List<Comment>>{
        _networkState.postValue(NetworkState.LOADING)
        jobComments = Job()
        return object : LiveData<List<Comment>>() {
            override fun onActive() {
                super.onActive()
                jobComments?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try {
                            val comment = apiService.getCommentById(issueId)
                            withContext(Dispatchers.Main) {
                                value = comment
                                Log.e(TAG, "Comments size-> ${value?.size}")
                                Log.e(TAG, "Comments -> $value")
                                _networkState.postValue(NetworkState.LOADED)
                                theJob.complete()
                            }

                        } catch (throwable: Throwable) {
                            _networkState.postValue(NetworkState.ERROR)
                            Log.e(TAG, "Error -> ${throwable.message}")
                        }
                    }
                }
            }
        }
    }

    /**
     * This function will cancel all the job associated with the issues
     */
    fun cancelJobIssue() {
        jobIssue?.cancel()
    }

    /**
     * This function will cancel all the job associated with the comments
     */
    fun cancelJobComments() {
        jobComments?.cancel()
    }
}