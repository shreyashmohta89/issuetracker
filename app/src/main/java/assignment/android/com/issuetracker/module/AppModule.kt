/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 7:06 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 10:47 AM
 *
 */
package assignment.android.com.randomuserapp.module

import assignment.android.com.issuetracker.data.viewmodel.CommentViewModel
import assignment.android.com.issuetracker.data.viewmodel.IssueViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel {
        IssueViewModel(get())
    }

    viewModel {
        CommentViewModel(get())
    }

}