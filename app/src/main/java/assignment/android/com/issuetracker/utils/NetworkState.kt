/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 9:28 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 6:52 PM
 *
 */
package assignment.android.com.issuetracker.utils

enum class Status {
    RUNNING, SUCCESS, FAILED

}

class NetworkState(val status: Status, val message: String) {

    companion object {

        val LOADED: NetworkState =
            NetworkState(
                Status.SUCCESS,
                "Success"
            )
        val LOADING: NetworkState =
            NetworkState(
                Status.RUNNING,
                "Running"
            )
        val ERROR: NetworkState =
            NetworkState(
                Status.FAILED,
                "Something went wrong"
            )
        val ENDOFLIST: NetworkState =
            NetworkState(
                Status.FAILED,
                "End game"
            )

    }


}