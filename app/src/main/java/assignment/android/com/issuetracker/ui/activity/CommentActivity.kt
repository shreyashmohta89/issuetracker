/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 7:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 7:54 PM
 *
 */

package assignment.android.com.issuetracker.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import assignment.android.com.issuetracker.R
import assignment.android.com.issuetracker.utils.NetworkState
import assignment.android.com.issuetracker.data.viewmodel.CommentViewModel
import assignment.android.com.issuetracker.ui.adapter.CommentAdapter

import assignment.android.com.issuetracker.utils.ISSUE_NUMBER
import kotlinx.android.synthetic.main.activity_comment.*
import org.koin.android.viewmodel.ext.android.viewModel

class CommentActivity : AppCompatActivity() {

    //Injecting the dependence of Koin Modules
    private val viewModel by viewModel<CommentViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        setContentView(R.layout.activity_comment)

        val issueId: Int = intent.getIntExtra(ISSUE_NUMBER, -1)

        viewModel.getCommentById(issueId)

        initListener()

        comment_recycler_view.layoutManager = LinearLayoutManager(this)


    }

    private fun initListener() {
        viewModel.commentList.observe(this, Observer {

            if(it.isEmpty()){
                comment_recycler_view.visibility = View.GONE
                tv_no_comments.visibility = View.VISIBLE

            }
            comment_recycler_view.adapter = CommentAdapter(this, it)
            setToolbarTitle(it.size)
        })

        viewModel.networkState.observe(this, Observer {
            comment_progress_bar.visibility =
                if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            comment_txt_error.visibility = if (it == NetworkState.ERROR) {
                View.VISIBLE
            } else {
                View.GONE
            }

        })

    }

    private fun setToolbarTitle(commentCount: Int) {

        val commentTitle = if (commentCount > 1) {
            getString(R.string.app_comments)
        }
        else getString(R.string.app_comment)

        title = "$commentTitle (${commentCount})"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.cancelJobsComment()
    }
}