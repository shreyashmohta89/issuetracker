/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 6:45 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 6:45 PM
 *
 */

package assignment.android.com.issuetracker.data.api

import assignment.android.com.issuetracker.data.model.Comment
import assignment.android.com.issuetracker.data.model.Issue
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService{

    @GET("issues")
    suspend fun getIssues() : List<Issue>

    @GET("issues/{issue_id}/comments")
    suspend fun getCommentById(@Path("issue_id")id: Int) : List<Comment>

}