/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 7:24 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 7:05 PM
 *
 */

package assignment.android.com.issuetracker.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import assignment.android.com.issuetracker.R
import assignment.android.com.issuetracker.data.viewmodel.IssueViewModel
import assignment.android.com.issuetracker.utils.NetworkState
import assignment.android.com.issuetracker.ui.adapter.IssueAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class IssueActivity : AppCompatActivity() {

    //Injecting the dependence of Koin Modules
    private val viewModel by viewModel<IssueViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.getIssues()

        initListener()

        recycler_view.layoutManager = LinearLayoutManager(this)
    }

    private fun initListener() {
        viewModel.issueList.observe(this, Observer {

            //Sort the list based on updated time and setting the value in issue adapter
            recycler_view.adapter = IssueAdapter(this,  it.sortedByDescending { it.updatedAt })
            setToolbarTitle(it.size)
        })

        viewModel.networkState.observe(this, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) {
                View.VISIBLE
            } else {
                View.GONE
            }

        })

    }

    private fun setToolbarTitle(issueCount: Int) {
        val issueTitle = if (issueCount > 1) {
            getString(R.string.app_issues)
        }
        else getString(R.string.app_issue)

        supportActionBar?.subtitle = "$issueTitle (${issueCount})"

    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.cancelJobsIssue()
    }
}
