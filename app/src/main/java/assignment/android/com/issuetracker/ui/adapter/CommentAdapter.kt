/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 8:25 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 8:25 PM
 *
 */

package assignment.android.com.issuetracker.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import assignment.android.com.issuetracker.R
import assignment.android.com.issuetracker.data.model.Comment
import assignment.android.com.issuetracker.databinding.ViewCommentItemBinding

class CommentAdapter(val context: Context, var commentList: List<Comment>?) :
    RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {

        val viewCommentItemBinding: ViewCommentItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.view_comment_item, parent, false
        )
        return CommentViewHolder(viewCommentItemBinding.root)
    }

    override fun getItemCount(): Int {
        return commentList!!.size
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val commentInfo: Comment = commentList!![position]
        holder.itemViewBinding?.comment = commentInfo


    }

    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemViewBinding = DataBindingUtil.bind<ViewCommentItemBinding>(itemView)


    }

}