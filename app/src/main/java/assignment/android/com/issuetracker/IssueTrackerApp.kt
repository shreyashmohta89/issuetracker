/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 7:05 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 7:05 PM
 *
 */

package assignment.android.com.issuetracker

import android.app.Application
import assignment.android.com.randomuserapp.module.apiModule
import assignment.android.com.randomuserapp.module.appModule
import assignment.android.com.randomuserapp.module.repoModule
import assignment.android.com.randomuserapp.module.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class IssueTrackerApp : Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@IssueTrackerApp)
            modules(arrayListOf(appModule, apiModule, retrofitModule, repoModule))
        }

    }
}