/*
 * *
 *  * Created by Shreyash Mohta on 1/3/20 9:29 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 1/3/20 9:29 PM
 *
 */

package assignment.android.com.issuetracker.data.repo


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import assignment.android.com.issuetracker.data.api.ApiService
import assignment.android.com.issuetracker.data.model.Comment
import assignment.android.com.issuetracker.data.model.Issue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate

@RunWith(MockitoJUnitRunner::class)
@PowerMockRunnerDelegate(MockitoJUnitRunner::class)
class IssueTrackerRemoteDataSourceTest {

    private lateinit var issueTrackerRemoteDataSource: IssueTrackerRemoteDataSource

    @Mock
    lateinit var apiService: ApiService


    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        issueTrackerRemoteDataSource = IssueTrackerRemoteDataSource(apiService)
    }

    @Test
    fun getIssues_successTest() {

        val results = listOf<Issue>()
        CoroutineScope(Dispatchers.IO).launch {
            val issue =  apiService.getIssues()
            Mockito.`when`(issue).thenReturn(results)

            assertNotNull(issueTrackerRemoteDataSource.getIssues().value)

            issueTrackerRemoteDataSource.getIssues()

            assertEquals(
                issueTrackerRemoteDataSource.getIssues().value?.get(0)?.number, results.get(0).number
            )

        }

    }

    @Test
    fun getIssues_nullTest() {

        CoroutineScope(Dispatchers.IO).launch {
            val issue =  apiService.getIssues()
            Mockito.`when`(issue).thenReturn(null)

            assertNotNull(issueTrackerRemoteDataSource.getIssues().value)

            issueTrackerRemoteDataSource.getIssues()

            assertEquals(
                issueTrackerRemoteDataSource.getIssues().value?.get(0)?.number, null
            )

        }

    }


    @Test
    fun getCommentById_successTest() {

        val results = listOf<Comment>()
        CoroutineScope(Dispatchers.IO).launch {
            val issue =  apiService.getCommentById(4997)
            Mockito.`when`(issue).thenReturn(results)

            assertNotNull(issueTrackerRemoteDataSource.getCommentById(4997).value)

            issueTrackerRemoteDataSource.getCommentById(4997)

            assertEquals(
                issueTrackerRemoteDataSource.getCommentById(4997).value?.get(0)?.user?.login,
                results.get(0).user.login
            )

        }

    }
}